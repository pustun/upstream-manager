angular
    .module('manager', ['ngRoute'])
    .controller('OverviewController', function($scope, upstreamsService) {

        function mapResponseToViewModel(apps) {
            return apps.map(function(app) {
                return {
                    name: app.name,
                    nodesState: app.nodes.map(function (node) {
                        return node.state;
                    })
                    .join(' | ')
                    .toUpperCase()
                };
            });
        }

        upstreamsService.getApplications()
            .then(function(apps) {
                return mapResponseToViewModel(apps);
            })
            .then(function(viewModel) {
                $scope.apps = viewModel;
            });
    })
    .controller('ManagementController', function($scope, $timeout, upstreamsService) {

        $scope.apps = [];

        $scope.currentApp = null;

        $scope.showErrorAlert = false;
        $scope.showSuccessAlert = false;

        $scope.selectApp = function(app) {
            $scope.currentApp = app2Dto(app);
        };

        $scope.nothingToSend = function(currentApp) {
            return currentApp && !currentApp.nodes.some(function(node) {
                return node.selected;
            });
        };

        $scope.sendCommand = function(currentApp, command) {

            var cmd = {
                application: currentApp.name,
                nodes: currentApp.nodes
                    .filter(function(node) {
                        return node.selected;
                    })
                    .map(function (node) {
                        return node.id;
                    }),
                command: command
            };

            hideAlerts();

            upstreamsService.adjustApplication(cmd)
                .then(function () {
                    showSuccessfulAlert();

                    resetCurrentAppSelection(currentApp);
                })
                .catch(function() {
                     showErrorAlert();
                });
        };


        $scope.isActive = function(appName) {
            return $scope.currentApp.name == appName;
        };

        function showErrorAlert() {
            $scope.showErrorAlert = true;

            $timeout(function () {
                $scope.showErrorAlert = false;
            }, 1500);
        }

        function showSuccessfulAlert() {
            $scope.showSuccessAlert = true;

            $timeout(function () {
                $scope.showSuccessAlert = false;
            }, 1500);
        }

        function hideAlerts() {
            $scope.showErrorAlert = $scope.showSuccessAlert = false;
        }

        function resetCurrentAppSelection(currentApp) {
            currentApp.nodes.forEach(function (node) {
                node.selected = false;
            });
        }

        function app2Dto(item) {
            return {
                name: item.name,
                nodes: item.nodes.map(function (node) {
                    return {
                        id: node.id,
                        selected: false
                    }
                })
            };
        }

        upstreamsService.getApplications()
            .then(function (apps) {
                $scope.apps = apps;

                var firstApp = apps[0];

                $scope.currentApp = app2Dto(firstApp)
            })

    })
    .controller('NavigationController', function($scope, $location) {
        $scope.currentUrl = $location.path();

        $scope.menu = [{
            text: 'Brief info',
            url: '/'
        }, {
           text: 'Manage nodes',
            url: '/management'
        }];

        $scope.isActive = function (url) {
            return $scope.currentUrl == url;
        };

        $scope.$watch(function() {
            return $location.path();
        }, function(value) {
           $scope.currentUrl = value;
        });
    })
    .factory('upstreamsService', function($http) {
        return {
            getApplications: getApplications,
            adjustApplication: adjustApplication
        };

        function adjustApplication(command) {
            return $http.post('/adjust', command);
        }

        function getApplications() {
            return $http.get('/status')
                .then(function(response) {
                    return response.data;
                });
        }
    })
    .config(function($routeProvider, $httpProvider) {

        $routeProvider.when('/', {
            controller: 'OverviewController',
            templateUrl: './views/overview.html'
        })
        .when('/management', {
            controller: 'ManagementController',
            templateUrl: './views/management.html'
        })
        .otherwise('/');

        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    });
