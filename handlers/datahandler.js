var upstreamWebProxy = require('../services/upstreamWebProxy');
var upstreamDtoMapper = require('../services/upstreamDtoMapper');

function getData(req, res) {
    upstreamWebProxy.fetchUpstreams()
        .then(function (rawServerData) {
           return upstreamDtoMapper.mapDtos(rawServerData);
        })
        .then(function(data) {
           res.json(data);
        });
}

function adjustApplication(req, res) {

    upstreamWebProxy.adjusUpstream(req.body)
        .then(function () {
           res.status(200).end();
        });
}

module.exports.getData = getData;
module.exports.adjustApplication = adjustApplication;