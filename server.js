var express = require('express');
var bodyParser = require('body-parser')

var datahandler = require('./handlers/datahandler');

var app = express();

app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

app.get('/', function(req, res) {
    res.sendFile('index.html', {root: './public'});
});

app.post('/adjust', datahandler.adjustApplication);

app.get('/status', datahandler.getData);

var server = app.listen(3000);