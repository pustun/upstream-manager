function mapDtos (rawDataFromServer) {
    var response = Object.keys(rawDataFromServer)
        .map(function(appKey) {
            var nodesArray = rawDataFromServer[appKey];

            return {
                name: appKey,
                nodes: nodesArray.map(function (node) {
                    return {
                        id: node.id,
                        state: node.state
                    };
                })
            };
        });

    return response;
}

module.exports.mapDtos = mapDtos;