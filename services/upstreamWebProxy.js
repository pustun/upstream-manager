var http = require('http');
var querystring = require('querystring');

var hostName = 'hr-stg-lb1.sachit.intern';

function fetchUpstreams() {

    var promise = new Promise(function(resolve, reject) {

        var options = {
            host: hostName,
            path: '/status/upstreams'
        };

        var req = http.get(options, function(res) {
            res.setEncoding('utf8');
            var body = '';

            res.on('data', function (chunk) {
                body += chunk;
            });

            res.on('end', function() {
                var result = JSON.parse(body);

                resolve(result);
            });
        });
        req.on('error', function(e) {
            reject(e);
        });

    });

    return promise;
};

function adjustUpstream(command) {
    var httpResponsePromises = command.nodes.map(function(node) {
        var promise = new Promise(function(resolve, reject) {

            var params = {};

            params.upstream = command.application;
            params.id = node;
            params[command.command] = 1

            var qs = querystring.stringify(params);

            var options = {
                host: hostName,
                path: '/upstream_conf?' + qs
            };

            var req = http.get(options, function(res) {
                resolve();
            });

            req.on('error', function(e) {
                reject(e);
            });
        });

        return promise;
    });

    return Promise.all(httpResponsePromises);
};

module.exports.fetchUpstreams = fetchUpstreams;
module.exports.adjusUpstream = adjustUpstream;